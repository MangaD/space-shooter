﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	public GameObject[] hazards;
	public Vector3 spawnValue;
	public int hazardCount;
	public float spawnWait, startWait, waveWait;
	public Text scoreText, restartText, gameOverText;

	private bool gameOver, restart;
	private uint score;

	void Start ()
	{
		foreach (var hazard in hazards) {
			hazard.GetComponent<Mover> ().speed = -15;
		}
		restartText.text = "";
		gameOverText.text = "";
		UpdateScore ();
		StartCoroutine (SpawnWaves ());
	}

	void Update ()
	{
		if (restart && Input.GetKeyDown (KeyCode.R)) {
			SceneManager.LoadScene ("Main");
		} else if (Input.GetKeyDown (KeyCode.P)) {
			Time.timeScale = (Time.timeScale == 0 ? 1 : 0);
		}
	}

	IEnumerator SpawnWaves () 
	{
		yield return new WaitForSeconds (startWait);
		while (true) {
			for (int i = 0; i < hazardCount; i++) {
				GameObject hazard = hazards[Random.Range(0, hazards.Length)];
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValue.x, spawnValue.x), spawnValue.y, spawnValue.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (hazard, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
			}
			foreach (var hazard in hazards) {
				hazard.GetComponent<Mover> ().speed -= 1;
			}
			hazardCount++;
			yield return new WaitForSeconds (waveWait);
			if (gameOver) {
				restartText.text = "Press 'R' for Restart";
				restart = true;
				break;
			}
		}
	}

	public void AddScore (uint newScore)
	{
		score += newScore;
		UpdateScore ();
	}

	void UpdateScore ()
	{
		scoreText.text = "Score: " + score;
	}

	public void GameOver ()
	{
		gameOverText.text = "Game Over!";
		gameOver = true;
		StartCoroutine (PlayLaugh ());
	}

	IEnumerator PlayLaugh ()
	{
		yield return new WaitForSeconds (2);
		GetComponent<AudioSource> ().Play ();
	}
}
