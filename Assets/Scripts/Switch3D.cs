﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch3D : MonoBehaviour {

	private GameObject cameraObj;
	private Camera cam;

	void Start() {
		cameraObj = GameObject.FindWithTag ("MainCamera");
		cam = (Camera)cameraObj.GetComponent (typeof(Camera));
	}

	void Update() {
		if (Input.GetKeyDown ("tab")) {
			if (cam.orthographic) {
				cam.orthographic = false;
				cam.transform.Rotate(0.0f, -180.0f, 0.0f, Space.World);
			} else {
				cam.orthographic = true;
				cam.transform.Rotate(0.0f, 180.0f, 0.0f, Space.World);
			}
		}
	}
}
