# Space Shooter

**Demo:** [https://mangad.ddns.net/games/SpaceShooter/](https://mangad.ddns.net/games/SpaceShooter/)

Unity tutorial of Space Shooter game with a few tweaks.

### Resources

* **[Tutorial](https://unity3d.com/pt/learn/tutorials/s/space-shooter-tutorial)**

* **[Space Ship](https://free3d.com/3d-model/wraith-raider-starship-22193.html)** - by Herminio Nieves
